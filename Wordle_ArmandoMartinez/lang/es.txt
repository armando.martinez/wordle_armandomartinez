﻿INSTRUCTIONS

Bienvenido a 
WORDLE!
En este juego deberás tratar de adivinar una palabra secreta.Para ello introduciras palabras y recibirás pistas.
El color verde marca que has acertado con esa letra, buen trabajo!
El color amarillo marca que esta letra está en la palabra, pero no en esta posición.
El color gris marca que la letra no se encuentra en esta palabra.
El juego terminará cuando adivines la palabra, o te quedes sin intentos. Diviertete!
