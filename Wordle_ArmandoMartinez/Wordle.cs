﻿using System;
using System.IO;

namespace Wordle_ArmandoMartinez
{
    class Wordle
    {
        static void Main()
        {
            Directory.SetCurrentDirectory(@"../../../");
            DrawAscii(@"./banner.txt", ConsoleColor.Green);
            string[] txt = File.ReadAllLines(GetLang());
            int option;
            do
            {
                option = Menu(new string[] { "1 - Jugar ", "2 - Instrucciones ", "3 - Estadisticas ",
                                             "4 - Borrar datos ", "5 - Configuración ", "0 - Exit " });
                switch (option)
                {
                    case 1:
                        break;
                    case 2:
                        Instructions(txt);
                        break;
                    case 3:
                        Stats();
                        break;
                    case 4:
                        DeleteSave();
                        break;
                    case 5:

                        break;
                    case 0:
                        Color(ConsoleColor.Black, ConsoleColor.Cyan);
                        Console.WriteLine("\n[EL PROGRAMA SE CERRARÁ...]");
                        Color();
                        break;
                }
            } while (option != 0);
            Pause();
        }

        // Mostra un menú en base a un array d'opcions
        static int Menu(string[] options)
        {
            Console.WriteLine();
            Color(ConsoleColor.Cyan);
            foreach (string option in options) { Console.WriteLine(option); }
            Color();
            int selectedOption = ParseIntInRange("\nSELECCIONA UNA OPCIÓ: ", 0, options.Length);
            return selectedOption;
        }

        // Demana una dada que obligatoriament ha d'estar dins d'un rang. Repeteix fins que es dona una dada adequada i la retorna.
        static int ParseIntInRange(string prompt, int min, int max)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum > max || parsedNum < min)
                {
                    Console.WriteLine($"El número ha d'estar entre {min} i {max}!");
                }
            } while (!isParsed || parsedNum > max || parsedNum < min);
            return parsedNum;
        }

        // Crea una pausa en el código
        static void Pause(string prompt = "\n[PRESS ENTER TO CONTINUE]\n")
        {
            Color(ConsoleColor.Black, ConsoleColor.Cyan);
            Console.Write(prompt);
            Color();
            Console.ReadLine();
        }

        // Cambia el color de la consola
        static void Color(ConsoleColor fg = ConsoleColor.White, ConsoleColor bg = ConsoleColor.Black)
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
            Console.Write("");
        }
        
        // Dibuja un ascii desde un fichero
        static void DrawAscii(string path, ConsoleColor fg = ConsoleColor.White, ConsoleColor bg = ConsoleColor.Black)
        {
            Color(fg, bg);
            string ascii = File.ReadAllText(path);
            Console.WriteLine(ascii);
            Color();
        }
        
        // Retorna el llenguatge que s'especifica al fitxer conf.txt 
        static string GetLang()
        {
            switch (File.ReadAllLines(@"./conf.txt")[1][5])
            {
                case '1':
                    return "./lang/cat.txt";
                case '2':
                    return "./lang/en.txt";
                case '3':
                    return "./lang/es.txt";
                default:
                    return "";
            }
        }

        // Muestra las instrucciones del juego
        static void Instructions(string[] txt)
        {
            Console.Write(txt[2]);
            Color(ConsoleColor.Green);
            Console.Write(txt[3]); Color();
            Console.WriteLine($"\n\n{txt[4]}");
            Color(ConsoleColor.Black, ConsoleColor.Green);
            Console.WriteLine(txt[5]);
            Color(ConsoleColor.Black, ConsoleColor.Yellow);
            Console.WriteLine(txt[6]);
            Color(ConsoleColor.Black, ConsoleColor.Gray);
            Console.WriteLine(txt[7]); Color();
            Console.WriteLine(txt[8]);
        }
        
        // Muestra las estadísticas de juego
        static void Stats()
        {
            StreamReader sr = new StreamReader(@"./save.txt");
            string[] data = sr.ReadToEnd().Split('\n');
            sr.Close();
            int playedGames = Convert.ToInt32(data[0].Split(':')[1]);
            int wonGames = Convert.ToInt32(data[1].Split(':')[1]);
            int lostGames = Convert.ToInt32(data[2].Split(':')[1]);
            string[] turnsUsed = data[3].Split(':')[1].Split(' ');
            Console.WriteLine($"\nHas jugat un total de {playedGames} partides.\nD'aquestes has guanyat {wonGames} i perdut {lostGames}.");
            double sum = 0;
            foreach (string n in turnsUsed)
            {
                if (n != "0") sum += Convert.ToInt32(n);
            }
            if (wonGames > 0) Console.WriteLine($"De mitjana has necessitat {Math.Round(sum / wonGames, 2)} intents per encertar.");
        }
        
        // Borra los datos de juego
        static void DeleteSave()
        {
            StreamWriter sw = new StreamWriter(@"./save.txt");
            sw.Write("games:0\nwon:0\nlost:0\nturns:0");
            sw.Close();
            Color(ConsoleColor.Black, ConsoleColor.Cyan);
            Console.WriteLine("\n[DATOS BORRADOS EXITOSAMENTE]");
            Color();
        }
    }
}
